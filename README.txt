Overview:
--------
Converts Ubercart INR price to USD prior to Paypal WPS payment and backward after Paypal IPN.

Installation:
--------
Enable the module. Setup Paypal WPS as usual.


Requires:
--------
 - Drupal 7.x
 - Ubercart 3.x

Credits:
-------
Copyright drupal-coder.ru 2012
Sponsored by moigeroi.ru
